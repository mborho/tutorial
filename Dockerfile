FROM debian:wheezy
MAINTAINER Martin Borho <martin@borho.net>

# Install packages required
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update -qq && apt-get install -y python-pip && apt-get clean

# Create a mount point
VOLUME ["/src/app"]

# Change work dir for the following commands
WORKDIR /src/app

# Copy requirements.txt first will avoid cache invalidation
COPY requirements.txt /src/app/ 

# Install python dependencies
RUN pip install -r ./requirements.txt

# Copy the sources into the image
COPY . /src/app/ 

# Run the container as 'www-data'
USER www-data

# Add the default config file for pypicloud
CMD [ "python", "./server.py" ]

